﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Priority_Queue;
using UnityEngine.UI;

public class Agent : MonoBehaviour {

	IDictionary<Vector3, Vector3> nodeParents = new Dictionary<Vector3, Vector3>();
    IDictionary<Vector3, Sprite> prevSprite = new Dictionary<Vector3, Sprite>();
    public IDictionary<Vector3, bool> walkablePositions; 
	public IDictionary<Vector3, string> obstacles; 
	

	NodeNetworkCreator nodeNetwork;
	IList<Vector3> path;

    public InputField weigS;
    public InputField weigvS;
    bool solutionVisible;
	string prevAlgo;
    int w, g, k;
    Camera camera;

    bool moveCube = false;
	int i;

	// Use this for initialization
	void Start ()
    {
        weigS.text = "3";
        weigvS.text = "5";
        w = 3;
        g = 5;
        k = 1;
        camera = FindObjectOfType<Camera>();
        nodeNetwork = GameObject.Find ("NodeNetwork").GetComponent<NodeNetworkCreator> ();
		obstacles = GameObject.Find ("NodeNetwork").GetComponent<NodeNetworkCreator> ().obstacles;
		walkablePositions = nodeNetwork.walkablePositions;
	}

    public void Weight()
    {
        int.TryParse(weigS.text, out w);
        int.TryParse(weigvS.text, out g);
    }

	// Update is called once per frame
	void Update () {

        camera.transform.position = new Vector3(this.transform.position.x+7, this.transform.position.y+3, -10 );

		
		if (moveCube) {
            float speed = 25 / Weight(path[i]);
			float step = Time.deltaTime * speed;
			transform.position = Vector3.MoveTowards (transform.position, path[i], step);
			if (transform.position.Equals (path [i]) && i >= 0)
				i--;
			if (i < 0)
				moveCube = false;
		}
	}

   

	Vector3 FindShortestPathDijkstra(Vector3 startPosition, Vector3 goalPosition){

        
        uint nodeVisitCount = 0;
        float timeNow = Time.realtimeSinceStartup;

        //A priority queue containing the shortest distance so far from the start to a given node
        IPriorityQueue<Vector3, int> priority = new SimplePriorityQueue<Vector3, int>();

        //A list of all nodes that are walkable, initialized to have infinity distance from start
        IDictionary<Vector3, int> distances = walkablePositions
            .Where(x => x.Value == true)
            .ToDictionary(x => x.Key, x => int.MaxValue);

        //Our distance from the start to itself is 0
        distances[startPosition] = 0;
        priority.Enqueue(startPosition, 0);

        while (priority.Count > 0) {
            
            Vector3 curr = priority.Dequeue();
            
            nodeVisitCount++;

           if (curr == goalPosition) {
                
                return goalPosition;
            }

			IList<Vector3> nodes = GetWalkableNodes (curr);

			//Look at each neighbor to the node
			foreach (Vector3 node in nodes) {

                int dist = distances[curr] + Weight(node);
               
                //If the distance to the parent, PLUS the distance added by the neighbor,
                //is less than the current distance to the neighbor,
                //update the neighbor's paent to curr, update its current best distance
                if (dist < distances [node]) {
					distances [node] = dist;
					nodeParents [node] = curr;

                    if (!priority.Contains(node))
                    {
                        priority.Enqueue(node, dist);
                    }
                }
			}
		}

        return startPosition;
	}

	int Weight(Vector3 node) {
        
        if (obstacles.Keys.Contains(node)) {
			if (obstacles [node] == "slow") {
				return w;
			} else if (obstacles [node] == "verySlow") {
				return g;
			} else {
				return k;
			}
		} else {
			return k;
		}
	}

	
	

	bool CanMove(Vector3 nextPosition) {
		return (walkablePositions.ContainsKey (nextPosition) ? walkablePositions [nextPosition] : false);
	}

	public void DisplayShortestPath(string algorithm) {

		if (solutionVisible && algorithm == prevAlgo) {
			foreach (Vector3 node in path) {
				nodeNetwork.nodeReference [node].GetComponent<SpriteRenderer> ().sprite = prevSprite[node];
			}

			solutionVisible = false;
			return;
		}
			
		nodeParents = new Dictionary<Vector3, Vector3>();
		path = FindShortestPath (algorithm);

		if (path == null)
			return;

		Sprite exploredTile = Resources.Load <Sprite>("path 1");
		Sprite victoryTile = Resources.Load<Sprite> ("victory 1");
		Sprite dijkstraTile = Resources.Load<Sprite> ("dijkstra");

		foreach (Vector3 node in path) {
			
			prevSprite[node] = nodeNetwork.nodeReference [node].GetComponent<SpriteRenderer> ().sprite;

           
             if (algorithm == "Dijkstra")
            {
                nodeNetwork.nodeReference[node].GetComponent<SpriteRenderer>().sprite = dijkstraTile;
            }
            
		}

		nodeNetwork.nodeReference [path [0]].GetComponent<SpriteRenderer> ().sprite = victoryTile;

		i = path.Count - 1;

		solutionVisible = true;
		prevAlgo = algorithm;
	}

	public void MoveCube(){
		moveCube = true;
	}

	IList<Vector3> FindShortestPath(string algorithm){

		IList<Vector3> path = new List<Vector3> ();
		Vector3 goal;
        
            goal = FindShortestPathDijkstra(this.transform.localPosition, GameObject.Find("Goal").transform.localPosition);
        

		if (goal == this.transform.localPosition || !nodeParents.ContainsKey(nodeParents[goal])) {
			
			return null;
		}

		Vector3 curr = goal;
		while (curr != this.transform.localPosition) {
			path.Add (curr);
			curr = nodeParents [curr];
		}

		return path;
	}

	IList<Vector3> GetWalkableNodes(Vector3 curr) {

		IList<Vector3> walkableNodes = new List<Vector3> ();

		IList<Vector3> possibleNodes = new List<Vector3> () {
			new Vector3 (curr.x + 1, curr.y, curr.z),
			new Vector3 (curr.x - 1, curr.y, curr.z),
			new Vector3 (curr.x, curr.y + 1, curr.z),
			new Vector3 (curr.x, curr.y - 1, curr.z),
            new Vector3 (curr.x + 1, curr.y + 1, curr.z),
            new Vector3 (curr.x + 1, curr.y - 1, curr.z),
            new Vector3 (curr.x - 1, curr.y + 1, curr.z),
            new Vector3 (curr.x - 1, curr.y - 1, curr.z)
        };

		foreach (Vector3 node in possibleNodes) {
			if (CanMove (node)) {
				walkableNodes.Add (node);
			} 
		}

		return walkableNodes;
	}
}
