﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class NodeNetworkCreator : MonoBehaviour
{

    public int boardWidth = 10;
    public int boardHeight = 10;
    public InputField Width;
    public InputField Height;
    public InputField numBariers;
    public InputField numSlow;
    public InputField numVerySlow;
    public InputField Startx;
    public InputField Starty;
    public InputField Finishx;
    public InputField Finishy;
    GameObject start;
    GameObject Finish;
    int a, b, c,d,e,f,g;

    public IDictionary<Vector3, bool> walkablePositions = new Dictionary<Vector3, bool>(); // массив мест без прохода
    public IDictionary<Vector3, GameObject> nodeReference = new Dictionary<Vector3, GameObject>();// массив для построения сетки
    public Dictionary<Vector3, string> obstacles = new Dictionary<Vector3, string>(); // разнообразность проходимых клеток грубо говоря вес

    //Use this for initialization

   void Start()
    {

        start = GameObject.Find("StartPoint");
        Finish = GameObject.Find("Goal");
        Width.text = "10";
        Height.text = "10";
        numBariers.text = "20";
        numSlow.text = "20";
        numVerySlow.text = "20";
        Startx.text = "0";
        Starty.text = "0";
        Finishx.text = "9";
        Finishy.text = "9";



     //  InitializeNodeNetwork(20, 20, 20);
    }

    //Update is called once per frame
    void Update()
    {

    }

    public void NewNetwork()
    {

          int.TryParse(Width.text, out boardWidth);
          int.TryParse(Height.text, out boardHeight);
          int.TryParse(numBariers.text, out a);
          int.TryParse(numSlow.text, out b);
          int.TryParse(numVerySlow.text, out c);
          int.TryParse(Startx.text, out d);
          int.TryParse(Starty.text, out e);
          int.TryParse(Finishx.text, out f);
          int.TryParse(Finishy.text, out g);

        start.transform.position = new Vector3(d, e, 0);
        Finish.transform.position = new Vector3(f, g, 0);
        InitializeNodeNetwork(a, b, c);




    }

    void InitializeNodeNetwork(int numBarriers, int numSlow, int numVerySlow)
    {

        var node = GameObject.Find("Node");
        var obstacle = GameObject.Find("Obstacle");
        var width = boardWidth;
        var height = boardHeight;

        obstacles = GenerateObstacles(numBarriers, numSlow, numVerySlow);

        Sprite slowTile = Resources.Load<Sprite>("obstacleslow1");
        Sprite verySlowTile = Resources.Load<Sprite>("obstacle_veryslow 1");
        

        for (int i = 0; i < width; i++) // цикл для создания объектов
        {
            for (int j = 0; j < height; j++)
            {
                Vector3 newPosition = new Vector3(i, j, 0);
                GameObject copy;
                string obstacleType = null;
                
                if (obstacles.TryGetValue(newPosition, out obstacleType))
                {
                    copy = Instantiate(obstacle);
                    copy.transform.position = newPosition;
                    switch (obstacleType) // проверка на проходимость и раскраска
                    {
                        case "barrier":
                            walkablePositions.Add(new KeyValuePair<Vector3, bool>(newPosition, false));
                            break;
                        case "slow":
                            walkablePositions.Add(new KeyValuePair<Vector3, bool>(newPosition, true));
                            copy.GetComponent<SpriteRenderer>().sprite = slowTile;
                            break;
                        case "verySlow":
                            walkablePositions.Add(new KeyValuePair<Vector3, bool>(newPosition, true));
                            copy.GetComponent<SpriteRenderer>().sprite = verySlowTile;
                            break;
                        
                    }
                }
                else
                {
                    copy = Instantiate(node);
                    copy.transform.position = newPosition;
                    walkablePositions.Add(new KeyValuePair<Vector3, bool>(newPosition, true));
                }

                nodeReference.Add(newPosition, copy);  
            }
        }

        GameObject goal = GameObject.Find("Goal");
        walkablePositions[goal.transform.localPosition] = true;
        nodeReference[goal.transform.localPosition] = goal;
    }

    Dictionary<Vector3, string> GenerateObstacles(int numBarriers, int numSlow, int numVerySlow)
    {
       

        for (int i = 0; i < numBarriers; i++)
        {
            Vector3 nodePosition = new Vector3(Random.Range(0, boardWidth), Random.Range(0, boardHeight), 0);
            if (!obstacles.ContainsKey(nodePosition))
            {
                obstacles.Add(nodePosition, "barrier");
            }
        }

        for (int i = 0; i < numSlow; i++)
        {
            Vector3 nodePosition = new Vector3(Random.Range(0, boardWidth), Random.Range(0, boardHeight), 0);
            if (!obstacles.ContainsKey(nodePosition))
            {
                obstacles.Add(nodePosition, "slow");
            }
        }

        for (int i = 0; i < numSlow; i++)
        {
            Vector3 nodePosition = new Vector3(Random.Range(0, boardWidth), Random.Range(0, boardHeight), 0);
            if (!obstacles.ContainsKey(nodePosition))
            {
                obstacles.Add(nodePosition, "verySlow");
            }
        }



        return obstacles;
    }
}
